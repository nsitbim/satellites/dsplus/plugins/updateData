const pluginDebug = "plugins:update";
const path = require("path");
const fs = require("fs");
var tasks = [];




async function updatePlugin(args, eventEmitter, [quality, debug]) {

  eventEmitter.on("init:update", (data, args) =>
    appendTasks(data, args, "update")
  );
  eventEmitter.on("init:delete", (data, args) =>
    appendTasks(data, args, "delete")
  );
  eventEmitter.on("end:init", processTasks);

  async function processTasks(args) {
    if (tasks.length > 0) {
      var target = path.join(path.resolve(args.options.target));
      var issuesFile = path.join(
        path.resolve(args.options.target),
        "../" + args.options.project + ".issues.json"
      );
      var overviewFile = path.join(
        path.resolve(args.options.target),
        "../" + args.options.project + ".overview.json"
      );
      var issuesHistoryFile = path.join(
        path.resolve(args.options.target),
        "../issues.history.json"
      );
      var overviewHistoryFile = path.join(
        path.resolve(args.options.target),
        "../overview.history.json"
      );
      var projectsFile = path.join(
        path.resolve(args.options.target),
        "../projects.json"
      );
      try {
        var issues = require(issuesFile);
      } catch (ex) {
        issues = { stats: {}, issues: [] };
      }
      try {
        var projects = require(projectsFile);
      } catch (ex) {
        projects = []
      }


      while (tasks.length) {
        
        var task = tasks.shift();
        if (!projects?.map(p=>p.name).includes(task.args.project)){
          projects.push({name: task.args.project})
          fs.writeFileSync(projectsFile,JSON.stringify(projects,null,2))
        }
        if (task.operation === "update") {
          issues = await updateAssets(task, issues);
        } else {
          issues = await deleteAssets(task, issues);
        }
      }

      args.options.external = [path.join(target, "/**")];
      args.options.metrics = true;
      var overview = quality.analyzeAsset(args.options);

      fs.writeFileSync(issuesFile, JSON.stringify(issues, null, 2));
      fs.writeFileSync(overviewFile, JSON.stringify(await overview, null, 2));

      var now = new Date();
      var issuesAgg = {
        date: now,
        project: args.options.project,
        issues: {},
      };
      issues.issues.map((i) => {
        issuesAgg.issues[i.severity] = (issuesAgg.issues[i.severity] || 0) + 1;
      });
      var overviewAgg = {
        date: now,
        project: args.options.project,
        metrics: (await overview)[0].metrics,
      };
      try {
        var issuesHistory = require(issuesHistoryFile);
      } catch (ex) {
        issuesHistory = [];
      }
      issuesHistory.push(issuesAgg);

      try {
        var overviewHistory = require(overviewHistoryFile);
      } catch (ex) {
        overviewHistory = [];
      }
      overviewHistory.push(overviewAgg);
      fs.writeFileSync(
        issuesHistoryFile,
        JSON.stringify(issuesHistory, null, 2)
      );
      fs.writeFileSync(
        overviewHistoryFile,
        JSON.stringify(overviewHistory, null, 2)
      );
    }
  }

  async function appendTasks(data, args, operation) {
    var task = {
      data: data,
      args: args,
      operation: operation,
    };
    tasks.push(task);
  }

  async function updateAssets(task, issues) {
    var data = task.data;
    var args = task.args;
    args.output = false;
    var assets = {};
    data.map((a) => {
      if (!assets[a.type]) {
        assets[a.type] = [];
      }
      assets[a.type].push(a.name);
    });
    args.asset = assets;
    issues.issues = issues.issues.filter((i) =>
      data.some((d) => d.name !== i.name)
    );
    issues.issues = issues.issues.concat(
      (await quality.analyzeAsset(args)).issues
    );

    return issues;
  }

  async function deleteAssets(task, issues) {
    var data = task.data;
    var args = task.args;

    var target = path.join(path.resolve(args.target));

    data.map((a) => {
      var assetPath = path.resolve(path.join(args.target, a.category, a.name));
      return fs.rmSync(assetPath, { recursive: true, force: true });
    });
    issues.issues = issues.issues.filter((i) =>
      data.some((d) => d.name !== i.name)
    );
    return issues;
  }
}

module.exports.update = updatePlugin;
